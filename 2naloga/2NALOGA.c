#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>

//Izpis slike
void slikice(int s){
    switch(s){
        case 0:
            printf("  +-0-+\n  |   |\n      |\n      |\n      |\n      |\n =========\n");
        break;
           
        case 1:
            printf("  +-1-+\n  |   |\n  0   |\n      |\n      |\n      |\n =========\n");
        break;

        case 2:
            printf("  +-2-+\n  |   |\n  0   |\n  |   |\n      |\n      |\n =========\n");
        break;

        case 3:
            printf("  +-3-+\n  |   |\n  0   |\n /|   |\n      |\n      |\n =========\n");
        break;

        case 4:
           printf("  +-4-+\n  |   |\n  0   |\n /|\\  |\n      |\n      |\n =========\n");
        break;

        case 5:
           printf("  +-5-+\n  |   |\n  0   |\n /|\\  |\n /    |\n      |\n =========\n");
        break;

        case 6:
           printf("  +-6-+\n  |   |\n  0   |\n /|\\  |\n / \\  |\n      |\n =========\n");
        break;
    }

}

//Preverja vpis
int validnost(char p){
    if(p>96 && p<123){
        return 1;
   }
    return 0;
}

int main(){
    //Izbere nakljucno ostevilo med 0 in 19
        srand(time(NULL));
        int x=rand()%20;

        char niz_besed[20][10]={"avtomobil","kamion","tovornjak","kolo","letalo","konj","sedlo","poljub","klop","gasilec","svizec","bor","smreka","jabolko","elipsa","eklipsa","albanija","srbija","maribor","slovenija"};
        char uganjujes[10];
        //shrani nakljucno besedo v sprejemljivko uganjujes
        strcpy(uganjujes,niz_besed[x]);
        int y=strlen(uganjujes);
        char beseda[y];
        //Napolni naso besedo ki bo displayana igralcu z "podcrtaji"
        strcpy(beseda,uganjujes);
        for(int i=0;i<y;i++){
            beseda[i]='_';
        }
        //shranjuje uporabljene crke v niz da nam jih bo prikazala
        char uporabljene_crke[20]="";
        int fa=0;
        char p;
        int check=0;
        printf("%s\n",beseda);
        printf("%s\n",uporabljene_crke);
        while(strcmp(beseda,uganjujes)==-1 && fa<7){
            //narise slikico iz funkcije
            slikice(fa);
            printf("Vnesi crko, vnasaj le male crke!!!\n");
            fflush(stdout);
            scanf(" %c",&p);
            //preverja ce si res vpisal malo crko
            while(validnost(p)==0){
            printf("Vnesi crko, vnasaj le male crke!!!\n");
            fflush(stdout);
            scanf(" %c",&p);
            }
            for(int i=0;i<y;i++){
                if(uganjujes[i]==p){
                    beseda[i]=p;
                    check++;
                }
            }
            if(check==0){
                printf("NAROBE!\n");
                uporabljene_crke[fa]=p;
                fa++;
            }
            check=0;
            printf("%s\n",beseda);
            printf("Napacne uporabljene crke: %s\n",uporabljene_crke);
        }
        //Rezultat
        if(fa>=7){
            printf("Spodletelo ti je\n");
        }
        else{
            printf("Uspelo ti je\n");
        }
return 0;
}